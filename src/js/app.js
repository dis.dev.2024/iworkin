"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import Sidebar from './components/sidebar.js'; // Sidebar
import NavMobile from './components/nav-mobile.js'; // Мобильное меню
import collapse from './components/collapse.js'; // Сворачиваемые блоки
import Rating from './components/rating.js'; // Rating plugin
import quantity from './forms/quantity.js' // input number
import Dropdown from "./components/dropdown.js";
import Spoilers from "./components/spoilers.js";
import Select from './components/select.js' // Select
import Options from './forms/options.js' // Options
import DataCheck from './components/data-check.js' // DataCheck
import Filter from './components/filter.js' // Select
// import AirDatepicker from 'air-datepicker'
import { Fancybox } from "@fancyapps/ui"; // Fancybox modal gallery
import * as bootstrap from 'bootstrap'

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// Плавный скролл
usefulFunctions.SmoothScroll('[data-anchor]')

// Сворачиваемые блоки
collapse();

// Маска для ввода номера телефона
maskInput('input[name="phone"]');

// input Number
quantity()

// Dropdown
Dropdown()

// Меню
Sidebar();

// Меню mobile
NavMobile();

// Spoilers
Spoilers();

// Rating
Rating();

// Select Dropdown
Select()

// Check table row
DataCheck()

// Filter

Filter()

Options()

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-choice]')) {

        event.target.closest('[data-choice-group]').querySelectorAll('[data-choice]').forEach(elem => {
            elem.classList.remove('active');
        });
        event.target.closest('[data-choice]').classList.add('active')
    }
})


// Currency
document.addEventListener('click', (event) => {
    if (event.target.closest('[data-currency-item]')) {
        event.target.closest('[data-dropdown]').classList.remove('open')
        event.target.closest('[data-currency-field]').querySelector('[data-currency-active]').innerHTML = event.target.closest('[data-currency-item]').innerHTML
    }
})

// Modal Fancybox
Fancybox.bind('[data-fancybox]', {
    autoFocus: false,
    closeButton: false
});

// password view

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-field-view]')) {
        event.preventDefault();
        event.target.closest('[data-field]').classList.toggle('pass-view')
    }
})

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-history-toggle]')) {
        event.preventDefault();
        event.target.closest('.history').classList.toggle('history-view')
    }
})

// Sliders
import "./components/sliders.js";

import "./components/datepicker.js";
