export default function() {


    window.addEventListener("DOMContentLoaded", function (e) {

        if (document.querySelectorAll('[data-check-item]').length > 0) {
            const dataCheckAllArray = document.querySelectorAll('[data-check-all]')
            let dataCheckAll = document.querySelector('[data-check-all]')
            let dataCheckItems = document.querySelectorAll('[data-check-item]')
            let dataCheckControl = document.querySelector('[data-control]')

            if (dataCheckAllArray.length > 0) {

                dataCheckAllArray.forEach((item) => {

                    item.addEventListener('change', (event) => {
                        if (item.checked) {
                            dataCheckAllArray.forEach(el => {
                                el.checked = true
                            })
                            dataCheckItems.forEach(el => {
                                el.checked = true
                            })
                            dataCheckControl.classList.add('active')
                            document.body.classList.add('data-control-visible')
                        }

                        else {
                            dataCheckAllArray.forEach(el => {
                                el.checked = false
                            })
                            dataCheckItems.forEach(el => {
                                el.checked = false
                            })
                            dataCheckControl.classList.remove('active')
                            document.body.classList.remove('data-control-visible')
                        }

                    })
                })

                dataCheckControl.addEventListener('change',  (event) => {

                    if (dataCheckAll.checked) {
                        dataCheckItems.forEach(el => {
                            el.checked = true
                            dataCheckControl.classList.add('active')
                            document.body.classList.add('data-control-visible')
                        })
                    }
                    else {
                        dataCheckItems.forEach(el => {
                            el.checked = false
                            dataCheckControl.classList.remove('active')
                            document.body.classList.remove('data-control-visible')
                        })
                    }
                })
            }

            dataCheckItems.forEach(elem => {
                elem.addEventListener('change', (event) => {

                    let counter = 0

                    dataCheckItems.forEach(el => {
                        if (el.checked) {
                            counter = counter + 1
                        }
                    })

                    if (counter > 0) {
                        if (counter < dataCheckItems.length) {
                            if (dataCheckAllArray.length > 0) {
                                dataCheckAllArray.forEach((item) => {
                                    item.indeterminate = true
                                })
                            }
                            dataCheckControl.classList.add('active')
                            document.body.classList.add('data-control-visible')
                        }
                        else {
                            if (dataCheckAllArray.length > 0) {
                                dataCheckAllArray.forEach((item) => {
                                    item.indeterminate = false
                                    item.checked = true
                                })
                            }
                            dataCheckControl.classList.add('active')
                            document.body.classList.add('data-control-visible')
                        }
                    }
                    else {
                        if (dataCheckAllArray.length > 0) {
                            dataCheckAllArray.forEach((item) => {
                                item.checked = false
                                item.indeterminate = false
                            })
                        }
                        dataCheckControl.classList.remove('active')
                        document.body.classList.remove('data-control-visible')
                    }
                })
            })

        }
    });
}
