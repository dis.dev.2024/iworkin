import  { toggle } from 'slidetoggle';

export default () => {
    document.addEventListener('click',  (event) => {
        if(event.target.closest('[data-nav-toggle]')) {
            document.body.classList.toggle('nav-mobile-open')
            document.querySelector('[data-nav-mobile]').classList.remove('nav-mobile-second')
            return false
        }
        if(event.target.closest('[data-nav-parents]')) {
            document.querySelector('[data-nav-mobile]').classList.toggle('nav-mobile-second')
            return false
        }

        if(event.target.closest('[data-parent-control]')) {
            event.preventDefault()

            // const navChild = event.target.closest('[data-parent]').querySelector('[data-child]');
            // toggle(
            //     navChild,
            //     {
            //         miliseconds: 200,
            //     }
            // )
            // event.target.closest('[data-parent]').classList.toggle('open')
        }

    })
};
