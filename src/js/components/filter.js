export default () => {

    if (document.querySelector('[data-filter]')) {
        const filterMenu = document.querySelectorAll('[data-filter-adding]');

        document.addEventListener('click', (event) => {
            if (filterMenu.length > 0) {
                if (event.target.closest('[data-filter-adding-toggle]')) {
                    let filterMenuContainer = event.target.closest('[data-filter-adding]');
                    if (filterMenuContainer.classList.contains('open')) {
                        filterMenuContainer.classList.remove('open');
                    }
                    else {
                        filterMenuClose();
                        filterMenuContainer.classList.add('open');
                    }
                }
                else {
                    if (!event.target.closest('[data-filter-adding-menu]')) {
                        filterMenuClose();
                    }
                }
                function filterMenuClose() {
                    filterMenu.forEach(el => {
                        el.classList.remove('open');
                    });
                }
            }
        });

        document.addEventListener('click', (event) => {
           if (event.target.closest('[data-filter-toggle]')) {
               event.target.closest('[data-filter]').classList.toggle('open');
               document.body.classList.toggle('filter-open');
           }
           if (event.target.closest('[data-filter-clear]')) {
               event.target.closest('[data-filter]').classList.remove('open');
               document.body.classList.remove('filter-open');
           }
        });
    }
};
