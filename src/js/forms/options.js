
export default () => {

    if (document.querySelectorAll('[data-options]').length > 0) {
        const optionsArray = document.querySelectorAll('[data-options]');

        document.addEventListener('click', (event) => {

            if (event.target.closest('[data-options-control]')) {
                let optionsContainer = event.target.closest('[data-options]');
                if (optionsContainer.classList.contains('open')) {
                    optionsContainer.classList.remove('open');
                }
                else {
                    optionsClose();
                    optionsContainer.classList.add('open');
                }
            }

            else {
                if (!event.target.closest('[data-options-dropdown]')) {
                    optionsClose();
                }
            }
        });

        document.addEventListener('click', (event) => {

            if (event.target.closest('[data-options-check-all]')) {
                let optionsItems = event.target.closest('[data-options]').querySelectorAll('[data-options-item]');
                let optionsValues = event.target.closest('[data-options]').querySelector('[data-options-values]')
                let optionsSelected = event.target.closest('[data-options]').querySelector('[data-options-selected]')
                let optionsSelectedArray = []
                let optionsSelectedHtml = ''
                let isNumeric = event.target.closest('[data-options]').classList.contains('options-easy')

                if (event.target.closest('[data-options-check-all]').classList.contains('checked')) {
                    event.target.closest('[data-options-check-all]').classList.remove('checked');
                    console.log('unchecked')
                    optionsItems.forEach(elem => {
                        elem.classList.remove('selected');
                    });
                    optionsSelectedArray = null;
                    optionsSelectedHtml = '';
                    event.target.closest('[data-options]').classList.remove('options--selected')
                }
                else {
                    event.target.closest('[data-options-check-all]').classList.add('checked');
                    console.log('checked')

                    optionsItems.forEach(elem => {
                        elem.classList.add('selected');
                        if (elem.dataset.optionsItem) {
                            optionsSelectedArray.push(elem.dataset.optionsItem)
                            optionsSelectedHtml += '<div class="options__choice">' + elem.dataset.optionsItem + '</div>'
                        }
                        else {
                            optionsSelectedArray.push(elem.innerHTML)
                            optionsSelectedHtml += '<div class="options__choice">' + elem.innerHTML + '</div>'
                        }
                    });
                    event.target.closest('[data-options]').classList.add('options--selected')
                }
                optionsValues.value = optionsSelectedArray;
                if (!isNumeric) {
                    optionsSelected.innerHTML = optionsSelectedHtml;
                }
                else {
                    if (optionsSelectedArray) {
                        event.target.closest('[data-options]').querySelector('[data-options-selected]').innerHTML =
                            '<div class="options__counter">' +
                            event.target.closest('[data-options]').querySelector('[data-options-label]').dataset.optionsLabel + ' - ' + optionsSelectedArray.length
                            + '</div>'
                    }
                    else {
                        event.target.closest('[data-options]').querySelector('[data-options-selected]').innerHTML = ''
                    }
                }
            }

            if (event.target.closest('[data-options-item]')) {
                let options = event.target.closest('[data-options]')
                let optionsSelectedArray = []
                let optionsSelectedHtml = ''
                let isNumeric = options.classList.contains('options-easy')
                console.log(isNumeric)
                event.target.closest('[data-options-item]').classList.toggle('selected');

                event.target.closest('[data-options]').querySelectorAll('.selected').forEach(elem => {
                    if (elem.dataset.optionsItem) {
                        optionsSelectedArray.push(elem.dataset.optionsItem)
                    }
                    else {
                        optionsSelectedArray.push(elem.innerHTML)
                    }
                });

                optionsSelectedArray.forEach(elem => {
                    optionsSelectedHtml += '<div class="options__choice">' + elem + '</div>'
                });

                if (optionsSelectedArray.length > 0) {
                    options.classList.add('options--selected')
                }
                else {
                    options.classList.remove('options--selected')
                }
                if (!isNumeric) {
                    options.querySelector('[data-options-selected]').innerHTML = optionsSelectedHtml
                }
                else  {
                    if (optionsSelectedArray.length > 0) {
                        options.querySelector('[data-options-selected]').innerHTML =
                            '<div class="options__counter">' +
                            options.querySelector('[data-options-label]').dataset.optionsLabel + ' - ' + optionsSelectedArray.length
                            + '</div>'
                    }
                    else {
                        options.querySelector('[data-options-selected]').innerHTML = ''
                    }
                }
                options.querySelector('[data-options-values]').value = optionsSelectedArray

                if (options.querySelectorAll('.selected').length === options.querySelectorAll('[data-options-item]').length) {
                    options.querySelector('[data-options-check-all]').classList.add('checked');
                }
                else {
                    options.querySelector('[data-options-check-all]').classList.remove('checked');
                }

            }
        });

        function optionsClose() {
            optionsArray.forEach(el => {
                el.classList.remove('open');
            });
        }
    }
};
