import AirDatepicker from "air-datepicker";
import {createPopper} from '@popperjs/core';

window.addEventListener("load", function (e) {
    document.querySelectorAll('[data-date]').forEach(el => {
        new AirDatepicker(el,{
            autoClose: true,
            range: true,
            multipleDatesSeparator: ' - '
        });
    });

    document.querySelectorAll('[data-range-date]').forEach(el => {
        new AirDatepicker(el,{
            autoClose: true,
            range: true,
            position: 'top left',
            multipleDatesSeparator: ' - '
        });
    });

    document.querySelectorAll('[data-month]').forEach(el => {
        new AirDatepicker(el,{
            autoClose: true,
            view: 'months',
            minView: 'months',
            dateFormat: 'MMMM yyyy',
            isMobile: true,
        });
    });
});
