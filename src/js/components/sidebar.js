export default () => {
    document.addEventListener('click',  (event) => {
        if(event.target.closest('[data-sidebar-toogle]')) {
            document.body.classList.toggle('sidebar-switched')
            return false
        }
        if(event.target.closest('[data-parent-control]')) {
            event.preventDefault()
            document.querySelector('[data-sidebar-sites]').classList.toggle('open')
            document.querySelector('.sidebar-overlay').classList.toggle('open')
            document.querySelector('[data-parent]').classList.toggle('active')
        }

    })
};

